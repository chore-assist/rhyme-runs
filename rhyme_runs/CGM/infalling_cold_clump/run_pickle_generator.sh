#!/bin/bash

mkdir -p pickle_logs

function max21 {
   while [ `jobs | wc -l` -ge 21 ]
   do
      sleep 1;
   done
}

declare -A p_names
p_names["55l_M05"]="l_M05-000000_chombo_h5.pickle"
p_names["55l_M1"]="l_M1-000000_chombo_h5.pickle"
p_names["55l_M2"]="l_M2-000000_chombo_h5.pickle"
p_names["55m_M05"]="m_M05-000000_chombo_h5.pickle"
p_names["55m_M1"]="m_M1-000000_chombo_h5.pickle"
p_names["55m_M2"]="m_M2-000000_chombo_h5.pickle"
p_names["55h_M05"]="h_M05-000000_chombo_h5.pickle"
p_names["55h_M1"]="h_M1-000000_chombo_h5.pickle"
p_names["55h_M2"]="h_M2-000000_chombo_h5.pickle"
p_names["56h_M05"]="h_M05-000000_chombo_h5.pickle"
p_names["56h_M1"]="h_M1-000000_chombo_h5.pickle"
p_names["56h_M2"]="h_M2-000000_chombo_h5.pickle"
p_names["57l_M05"]="l_M05-000002_chombo_h5.pickle"
p_names["57l_M1"]="l_M1-000002_chombo_h5.pickle"
p_names["57l_M2"]="l_M2-000002_chombo_h5.pickle"
p_names["57m_M05"]="m_M05-000001_chombo_h5.pickle"
p_names["57m_M1"]="m_M1-000001_chombo_h5.pickle"
p_names["57m_M2"]="m_M2-000001_chombo_h5.pickle"
p_names["57h_M05"]="h_M05-000000_chombo_h5.pickle"
p_names["57h_M1"]="h_M1-000000_chombo_h5.pickle"
p_names["57h_M2"]="h_M2-000000_chombo_h5.pickle"

for d in `ls | grep L5`; do
  for dd in `ls $d| grep 5`; do
    echo "- ${dd}"
    running=0
    for p in `pgrep -fa pickle_generator`; do
      if [[ $p == *"${d}/${dd}/${dd}.txt"* ]]; then
        running=1
      fi
    done
    if [[ "${running}" == "1" ]]; then
      echo "${d}/${dd}/${dd} is already running!";
    else
      echo "restarting ${d}/${dd}/${dd}!";
      # max21; ./pickle_generator.py ${d}/${dd}/${dd}.txt -U ./pickles/${d}/${dd}/output/${p_names[${dd}]} > ./pickle_logs/${dd}.log &
    fi
  done
done

wait
