#!/usr/bin/env python3

import shutil
from pathlib import Path

DATASET_PATH = '/net/theia/scratch/saeed/development/phd-thesis/notebooks/datasets'

PICKLES = {
    '55': {
        'l_M05': {'path': './pickles/L55.00Hz/55l_M05/output/l_M05-000000_chombo_h5.pickle'},
        'l_M1': {'path': './pickles/L55.00Hz/55l_M1/output/l_M1-000000_chombo_h5.pickle'},
        'l_M2': {'path': './pickles/L55.00Hz/55l_M2/output/l_M2-000000_chombo_h5.pickle'},
        'm_M05': {'path': './pickles/L55.00Hz/55m_M05/output/m_M05-000000_chombo_h5.pickle'},
        'm_M1': {'path': './pickles/L55.00Hz/55m_M1/output/m_M1-000000_chombo_h5.pickle'},
        'm_M2': {'path': './pickles/L55.00Hz/55m_M2/output/m_M2-000000_chombo_h5.pickle'},
        'h_M05': {'path': './pickles/L55.00Hz/55h_M05/output/h_M05-000000_chombo_h5.pickle'},
        'h_M1': {'path': './pickles/L55.00Hz/55h_M1/output/h_M1-000000_chombo_h5.pickle'},
        'h_M2': {'path': './pickles/L55.00Hz/55h_M2/output/h_M2-000000_chombo_h5.pickle'},
    },
    '56': {
        'h_M05': {'path': './pickles/L56.00Hz/56h_M05/output/h_M05-000000_chombo_h5.pickle'},
        'h_M1': {'path': './pickles/L56.00Hz/56h_M1/output/h_M1-000000_chombo_h5.pickle'},
        'h_M2': {'path': './pickles/L56.00Hz/56h_M2/output/h_M2-000000_chombo_h5.pickle'},
    },
    '57': {
        'l_M05': {'path': './pickles/L57.00Hz/57l_M05/output/l_M05-000002_chombo_h5.pickle'},
        'l_M1': {'path': './pickles/L57.00Hz/57l_M1/output/l_M1-000002_chombo_h5.pickle'},
        'l_M2': {'path': './pickles/L57.00Hz/57l_M2/output/l_M2-000002_chombo_h5.pickle'},
        'm_M05': {'path': './pickles/L57.00Hz/57m_M05/output/m_M05-000001_chombo_h5.pickle'},
        'm_M1': {'path': './pickles/L57.00Hz/57m_M1/output/m_M1-000001_chombo_h5.pickle'},
        'm_M2': {'path': './pickles/L57.00Hz/57m_M2/output/m_M2-000001_chombo_h5.pickle'},
        'h_M05': {'path': './pickles/L57.00Hz/57h_M05/output/h_M05-000000_chombo_h5.pickle'},
        'h_M1': {'path': './pickles/L57.00Hz/57h_M1/output/h_M1-000000_chombo_h5.pickle'},
        'h_M2': {'path': './pickles/L57.00Hz/57h_M2/output/h_M2-000000_chombo_h5.pickle'},
    },
}


def main():
    for simname, sim in PICKLES.items():
        for ver, pkl in sim.items():
            if Path(pkl['path']).exists():
                print(f"[Accessible] {simname}{ver} {pkl['path']}")
            else:
                print(f"[Error] {simname}{ver} {pkl['path']}")
            targetname = simname + '_' + ver + '_infalling.pickle'
            print('Copying ' + pkl['path'] + ' to ' + targetname + '...')
            shutil.copyfile(pkl['path'], DATASET_PATH + '/' + targetname)
            print('[done]')


if __name__ == '__main__':
    main()
