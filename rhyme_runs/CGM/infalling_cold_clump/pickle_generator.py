#!/usr/bin/env python3

import os
import re
import pickle
import argparse
import shutil
import gc
import sys
from pathlib import Path
from datetime import datetime
from math import floor
import time

import numpy as np
from astropy.units import Unit
import astropy.constants as cnst

from pyrhyme import PyRhyme


def main():
    parser = argparse.ArgumentParser(description='''
    Rhyme Pickle generator
    Extracting important information from Rhyme output!''')

    parser.add_argument('parameter_file', help='Path to the parameter file',
                        type=str)

    parser.add_argument('-U', '--update', help='Updating existing Pickle',
                        type=str, default=None)
    #  parser.add_argument('-S', '--step', help='Snapshot step',
    #                      type=int, default=None)
    #  parser.add_argument('-F', '--first', help='First snapshot to process',
    #                      type=int, default=None)
    #  parser.add_argument('-L', '--last', help='Last snapshot to process',
    #                      type=int, default=None)
    parser.add_argument('-B', '--backup-every',
                        help='Saving a backup every X steps [default: 5]',
                        type=int, default=5)

    args = parser.parse_args()

    param_file_path = args.parameter_file
    update = args.update
    #  step = args.step if args.step else 1
    backup_every = args.backup_every

    params = extract_params(param_file_path)
    chombo_path = params['chombo_path']
    dirname = params['dirname']
    filename = params['filename']
    ic_path = dirname + '/IC-000000.chombo.h5'

    # Openning chombo files
    first = PyRhyme(chombo_path)
    n_snaps = first.dataset.num_of_snapshots
    first.dataset.close_current()
    del first

    # NB: ic must be open after first (snapshot)
    ic = PyRhyme(ic_path)
    ic.dataset.jump_to(0)

    domain = ic.dataset.problem_domain

    attrs = ic.dataset.active['h5']['attrs']
    rho_x = ic.lineout([0, .5, .5], [1, .5, .5],
                       'rho', n_intervals=domain[0]-1)
    rho_y = ic.lineout([.5, 0, .5], [.5, 1, .5],
                       'rho', n_intervals=domain[0]-1)
    rho_z = ic.lineout([.5, .5, 0], [.5, .5, 1],
                       'rho', n_intervals=domain[0]-1)

    sphere = {
        'center': [
            (params['cx'] * Unit(params['unit'])).to(
                attrs['length_unit']).value,
            (params['cy'] * Unit(params['unit'])).to(
                attrs['length_unit']).value,
            (params['cz'] * Unit(params['unit'])).to(
                attrs['length_unit']).value,
        ],
        'r': (params['r'] * Unit(params['unit'])).to(
            attrs['length_unit']).value,
        'h': (params['h'] * Unit(params['unit'])).to(
            attrs['length_unit']).value,
        'unit': attrs['length_unit'],
    }

    if update is None:
        snapshots = {
            'chombo_path': chombo_path,
            'dirname': dirname,
            'filename': filename,
            'ic_path': ic_path,
            'time': datetime.now(),
            'problem_domain': domain,
            'num_of_snapshots': n_snaps,
            'density_unit': attrs['density_unit'],
            'length_unit': attrs['length_unit'],
            'pressure_unit': attrs['pressure_unit'],
            'temperature_unit': attrs['temperature_unit'],
            'time_unit': attrs['time_unit'],
            'velocity_unit': attrs['velocity_unit'],
            'x': [x[0] for x in rho_x['x']],
            'y': [x[1] for x in rho_y['x']],
            'z': [x[2] for x in rho_z['x']],
            'dx': ic.dataset.dx(0)[0] * attrs['length_unit'],
            'dy': ic.dataset.dx(0)[1] * attrs['length_unit'],
            'dz': ic.dataset.dx(0)[2] * attrs['length_unit'],
            'ic': None,
            'snaps': {},
            'logs': [
                ('20200805', 'First version of histograms'),
                ('20200817', 'Masked histograms'),
                ('20200914', 'Higher resolution histograms; Added T and p'),
                ('20201204', 'Selected snapshots'),
            ]
        }

        for key in snapshots.keys():
            if key in ['x', 'y', 'z']:
                print(key, '=>', snapshots[key][:5],
                      '...', snapshots[key][-5:])
                continue
            print(key, '=>', snapshots[key])
    else:
        print('Loading existing Pickle...')
        with open(update, 'rb') as p:
            snapshots = pickle.load(p, encoding='latin1')

    print('Loading snapshots...')

    # Histogram
    nbins = 512
    variables = ic.load_variables()
    ic_min_rho = np.min(variables['rho'][0]) / 2
    ic_max_rho = np.max(variables['rho'][0]) * 2
    db_log = (np.log10(ic_max_rho) - np.log10(ic_min_rho)) / nbins

    hist_info = {
        'nbins': nbins,
        'range': [ic_min_rho, ic_max_rho],
        'bins': [0.0] + [
            ic_min_rho * 10**(i * db_log) for i in range(0, nbins + 1)
        ] + [float('inf')],
        'db_log': db_log
    }

    if not snapshots['ic']:
        print('IC:')
        p, mu, T = ic.calc_temperature(variables)
        rec_rate, col_rate = ic.lyman_alpha_rates(
            variables, T, X=.75, Y=.25)
        rec, col = ic.lyman_alpha_emission(
            rec_rate, col_rate, variables, T, X=.75, Y=.25)
        profiles, NHI = ic.lyman_alpha_line_profile(
            rec_rate, col_rate, variables)

        snapshots['ic'] = read_snapshot(
            ic, variables, p, mu, T, rec, col, profiles,
            NHI, domain)

        snapshots['ic']['histograms'] = \
            generate_histograms(
                variables, rec, col, domain,
                snapshots['dx'].value, snapshots['dy'].value,
                snapshots['dz'].value, hist_info, sphere)

        store(dirname, filename, snapshots)

    snapshots['snaps'][0] = snapshots['ic']
    del ic

    n_saved_files = 0
    #  start = args.first if args.first else 0
    #  end = args.last if args.last else n_snaps

    start_time = time.time()
    print('- Preparation... ', end='')

    snap = PyRhyme(chombo_path)

    indices, times, snap_ids = [], [], []

    for i in range(snap.dataset.num_of_snapshots):
        try:
            snap.dataset.jump_to(i)
            indices.append(i)
            times.append(float(snap.dataset.time))

            if snap.dataset.time <= 0.105:
                snap_ids.append(i)

            snap.dataset.close_current()
        except:
            print(f'- [Err] unable to open {i}')

    print('[done in {} sec]'.format(int(time.time() - start_time)))

    print(f'time range: {np.min(times)} -- {np.max(times)}')
    times = np.array(times)
    snap_ids += [indices[np.argmax(times > x)]
                 for x in np.arange(0.25, np.max(times), 0.25)]

    print(times[snap_ids])
    print('- number of selected times = {}'.format(len(snap_ids)))

    for i in snap_ids:
        start_time = time.time()
        snap_idx = snap_ids.index(i)
        print('Snapshot ' + str(snap_idx + 1) +
              ' out of ' + str(len(snap_ids)))

        try:
            snap.dataset.jump_to(i)
        except:
            print(f'- [Err] Unable to open snapshot {i} with index {snap_idx}')
            continue

        iteration = snap.dataset.active['h5']['attrs']['iteration']
        print('  iteration: ' + str(iteration))
        print('  filepath: ' + str(snap.dataset.active['path']))

        if iteration not in snapshots['snaps'] or not snapshots['snaps'][iteration]:
            variables = snap.load_variables()
            p, mu, T = snap.calc_temperature(variables)
            rec_rate, col_rate = snap.lyman_alpha_rates(
                variables, T, X=.75, Y=.25)
            rec, col = snap.lyman_alpha_emission(
                rec_rate, col_rate, variables, T, X=.75, Y=.25)
            profiles, NHI = snap.lyman_alpha_line_profile(
                rec_rate, col_rate, variables)

            snapshots['snaps'][iteration] = read_snapshot(
                snap, variables, p, mu, T, rec, col, profiles,
                NHI, domain)

            n_saved_files += 1

        if 'histograms' not in snapshots['snaps'][iteration]:
            snapshots['snaps'][iteration]['histograms'] = \
                generate_histograms(
                    variables, rec, col, domain,
                    snapshots['dx'].value, snapshots['dy'].value,
                    snapshots['dz'].value, hist_info, sphere)

        if (n_saved_files + 1) % backup_every == 0:
            store(dirname, filename, snapshots, backup=True)
            gc.collect()
        elif (n_saved_files + 1) % 12 == 0:
            store(dirname, filename, snapshots)
            gc.collect()

        snap.dataset.close_current()

        print('- done in {} sec'.format(int(time.time() - start_time)))

    store(dirname, filename, snapshots, backup=True)


def extract_params(path):
    params = {}

    desired_params = {
        'chombo_prefix': {
            'dir': (2, lambda x: x.replace('"', '').replace("'", ''))
        },
        'shape = sphere': {
            'cx': (3, lambda x: float(x)),
            'cy': (4, lambda x: float(x)),
            'cz': (5, lambda x: float(x)),
            'r': (6, lambda x: float(x)),
            'h': (7, lambda x: float(x)),
            'unit': (8, lambda x: x.replace('"', '').replace("'", ''))
        }
    }

    param_file = open(path, 'r')
    param_lines = param_file.readlines()

    for line in param_lines:
        for param_name, param_values in desired_params.items():
            if line[:len(param_name)].replace(' ', '') \
                    == param_name.replace(' ', ''):
                line_split = line.split()
                for param_tag, transf in param_values.items():
                    params[param_tag] = transf[1](line_split[transf[0]])

    if any(p not in params for p in
            ['dir', 'cx', 'cy', 'cz', 'r', 'h', 'unit']):
        print('Something is missing in params!', params)
        sys.exit(0)

    params['dirname'] = os.path.dirname(path) + '/' + params['dir']

    file_list = sorted(os.listdir(params['dirname']))
    for f in file_list:
        if f[:2] != 'IC' and f[-10:] == '.chombo.h5':
            params['chombo_path'] = params['dirname'] + '/' + f
            break

    params['filename'] = os.path.basename(params['chombo_path'])

    return params


def generate_histograms(v, rec, col, domain, dx, dy, dz,
                        hist_info, sphere):
    print('- generating histogams...')

    hist = {
        'masked': {'interface': {}},
        'all': {}
    }

    rng = hist['range'] = hist_info['range']
    bins = hist['bin_edges'] = hist_info['bins']
    db_log = hist['db_log'] = hist_info['db_log']
    hist['nbins'] = hist_info['nbins']

    log_min = np.log10(rng[0])

    comps = ['rho', 'u', 'v', 'w', 'p', 'e_tot', 'temp',
             'ntr_frac_0', 'ntr_frac_1', 'ntr_frac_2']

    hist['all']['freq'] = np.zeros(len(bins) - 1, dtype=np.int32)
    hist['masked']['interface']['freq'] = np.zeros(
        len(bins) - 1, dtype=np.int32)

    for comp in comps:
        hist['masked']['interface'][comp] = np.zeros(
            len(bins) - 1, dtype=np.float64)
        hist['all'][comp] = np.zeros(len(bins) - 1, dtype=np.float64)

    hist['all']['emissivity'] = {}
    hist['masked']['interface']['emissivity'] = {}

    hist['all']['emissivity']['col'] = np.zeros(
        len(bins) - 1, dtype=np.float64)
    hist['all']['emissivity']['rec'] = np.zeros(
        len(bins) - 1, dtype=np.float64)
    hist['masked']['interface']['emissivity']['col'] = np.zeros(
        len(bins) - 1, dtype=np.float64)
    hist['masked']['interface']['emissivity']['rec'] = np.zeros(
        len(bins) - 1, dtype=np.float64)

    dX = np.array([dx, dy, dz])
    ll = sphere['r'] - 2 * sphere['h']
    ul = sphere['r'] + 2 * sphere['h']
    print('- dX: {}, {}, {}'.format(dX[0], dX[1], dX[2]))
    print(
        '- shell origin: {} [{}]'.format(sphere['center'], sphere['unit']))
    print('- masked shell: {} < r < {} [{}]'.format(ll, ul, sphere['unit']))

    _kB = cnst.k_B.cgs
    print('- kB: {} [{}]'.format(_kB.value, _kB.unit))

    def _fill_hist(_hist, i, _r, _rvx, _rvy, _rvz, _e, _T,
                   _fHI, _fHeI, _fHeII, _cr, _rr, X=.75, Y=.25):
        _hist['freq'][i] += 1
        _hist['rho'][i] += _r
        _hist['u'][i] += _rvx / _r if _r > 0.0 else 0.0
        _hist['v'][i] += _rvy / _r if _r > 0.0 else 0.0
        _hist['w'][i] += _rvz / _r if _r > 0.0 else 0.0
        _hist['e_tot'][i] += _e
        _hist['p'][i] += _r * (
            (X / 1.00784) * (2 - _fHI) +
            (Y / 4.002602) * (3 - 2 * _fHeI - _fHeII)
        ) * _kB.value * _T
        _hist['temp'][i] += _T
        _hist['ntr_frac_0'][i] += _fHI
        _hist['ntr_frac_1'][i] += _fHeI
        _hist['ntr_frac_2'][i] += _fHeII
        _hist['emissivity']['col'][i] += _cr
        _hist['emissivity']['rec'][i] += _rr

    progress = -1
    for k in range(domain[2]):
        if int(k / (domain[2] - 1) * 100) > progress:
            progress = int(k / (domain[2] - 1) * 100)
            print("- Creating histograms [{:3d}%]".format(progress),
                  end='\r' if progress < 100 else '\n', flush=True)

        for j in range(domain[1]):
            for i in range(domain[0]):
                idx = i + j * domain[0] + k * (domain[0] * domain[1])

                r = v['rho'][0][idx]
                rvx = v['rho_u'][0][idx]
                rvy = v['rho_v'][0][idx]
                rvz = v['rho_w'][0][idx]
                e = v['e_tot'][0][idx]
                T = v['temp'][0][idx]
                fHI = v['ntr_frac_0'][0][idx]
                fHeI = v['ntr_frac_1'][0][idx]
                fHeII = v['ntr_frac_2'][0][idx]
                cr = col[0][idx]
                rr = rec[0][idx]

                d = np.sqrt(sum(
                    (sphere['center'] - (np.array([i, j, k]) + .5) * dX)**2
                ))

                not_masked = d > ul or d < ll

                if r < rng[0]:
                    _fill_hist(hist['all'], 0, r, rvx, rvy,
                               rvz, e, T, fHI, fHeI, fHeII, cr, rr)
                    if not_masked:
                        _fill_hist(hist['masked']['interface'], 0, r, rvx,
                                   rvy, rvz, e, T, fHI, fHeI, fHeII, cr, rr)
                elif r > rng[1]:
                    _fill_hist(hist['all'], -1, r, rvx, rvy,
                               rvz, e, T, fHI, fHeI, fHeII, cr, rr)
                    if not_masked:
                        _fill_hist(hist['masked']['interface'], -1, r, rvx,
                                   rvy, rvz, e, T, fHI, fHeI, fHeII, cr, rr)
                else:
                    i = floor((np.log10(r) - log_min) / db_log) + 1

                    _fill_hist(hist['all'], i, r, rvx, rvy,
                               rvz, e, T, fHI, fHeI, fHeII, cr, rr)
                    if not_masked:
                        _fill_hist(hist['masked']['interface'], i, r, rvx,
                                   rvy, rvz, e, T, fHI, fHeI, fHeII, cr, rr)

    print('- masked pixels: {}'.format(
        sum(hist['all']['freq']) - sum(hist['masked']['interface']['freq'])))

    return hist


def read_snapshot(r, v, p, mu, T, rec, col,
                  profiles, NHI, domain):
    snap = {}

    snap['time'] = r.dataset.active['h5']['attrs']['time']
    snap['iteration'] = r.dataset.active['h5']['attrs']['iteration']

    m = [int(d / 2) for d in domain]

    for comp in r.dataset.active['h5']['attrs']['components']:
        snap[comp] = {}
        snap[comp]['line'] = np.einsum('iii->i', v[comp][0].reshape(domain, order='F'))
        snap[comp]['sheet'] = np.einsum('iij->ij', v[comp][0].reshape(domain, order='F'))

    snap['p'] = {
        'line': np.einsum('iii->i', p.reshape(domain, order='F')),
        'sheet': np.einsum('iij->ij', p.reshape(domain, order='F')),
    }
    snap['mu'] = {
        'line': np.einsum('iii->i', mu.reshape(domain, order='F')),
        'sheet': np.einsum('iij->ij', mu.reshape(domain, order='F')),
    }

    snap['T'] = {
        'line': np.einsum('iii->i', T[0].reshape(domain, order='F')),
        'sheet': np.einsum('iij->ij', T[0].reshape(domain, order='F')),
    }

    snap['sb'] = {}
    snap['sb']['unit'] = rec[1]

    snap['sb']['rec'] = {}
    snap['sb']['rec']['xy'] = np.array(
        np.sum(rec[0].reshape(domain, order='F'), axis=2), dtype=np.float64)
    snap['sb']['rec']['zx'] = np.array(
        np.sum(rec[0].reshape(domain, order='F'), axis=1), dtype=np.float64)
    snap['sb']['rec']['yz'] = np.array(
        np.sum(rec[0].reshape(domain, order='F'), axis=0), dtype=np.float64)

    snap['sb']['col'] = {}
    snap['sb']['col']['xy'] = np.array(
        np.sum(col[0].reshape(domain, order='F'), axis=2), dtype=np.float64)
    snap['sb']['col']['zx'] = np.array(
        np.sum(col[0].reshape(domain, order='F'), axis=1), dtype=np.float64)
    snap['sb']['col']['yz'] = np.array(
        np.sum(col[0].reshape(domain, order='F'), axis=0), dtype=np.float64)

    snap['NHI'] = NHI
    snap['line_profile'] = profiles

    return snap


def store(dirname, filename, snapshots, backup=False):
    directory, name = pickle_name(dirname, filename)
    filepath = directory + '/' + name

    print('Saving...')
    print('- directory: ' + directory)
    print('- filename: ' + name)
    print('- backup: ' + str(backup))

    Path(directory).mkdir(parents=True, exist_ok=True)

    bkp1 = filepath + '-1last'
    bkp2 = filepath + '-2last'

    if backup and Path(bkp2).is_file():
        print('- Removing the oldest backup')
        try:
            os.remove(bkp2)
        except OSError:
            print('- [ERR] Unable to delete file:', bkp2)

    if backup and Path(bkp1).is_file():
        print('- Replacing the latest backup with the oldest!')
        shutil.move(bkp1, bkp2)

    if backup and Path(filepath).is_file():
        print('- Replacing the current pickle file with the latest backup')
        shutil.move(filepath, bkp1)

    print('- Backing up:', filepath)
    with open(filepath, 'wb') as p:
        pickle.dump(snapshots, p, pickle.HIGHEST_PROTOCOL)


def pickle_name(dirname, filename):
    directory = './pickles/' + dirname

    name = re.sub(
        '^[^A-Za-z]*', '', filename.replace('/', '_').replace('.', '_')
    )
    name = re.sub('-[0-9]{5}_chombo_h5$', '', name) + '.pickle'

    return directory, name


if __name__ == '__main__':
    main()
