#!/usr/bin/env python3

from math import sqrt, log10
from pathlib import Path

from astropy.cosmology import Planck15 as cosmo
from astropy.units import km, cm, pc, kpc, Mpc, s, yr, Myr, K, Hz, Unit
from astropy.constants import k_B, m_p

PRESET_SIMS = {
    '55l_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 0.320563018979709 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0016028150948985452 * cm**-3, 'T': 1000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55l+_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 0.570050616271533 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.002137388280842101 * cm**-3, 'T': 1333521.432163324 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55lm_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 1.013709273595666 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0028502530813576637 * cm**-3, 'T': 1778279.410038923 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55lm+_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 1.8026583290006861 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.003800873571079999 * cm**-3, 'T': 2371373.7056616554 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55m_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 3.205630189797091 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.005068546367978328 * cm**-3, 'T': 3162277.6601683795 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55m+_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 5.700506162715331 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.006759015211612675 * cm**-3, 'T': 4216965.034285823 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55mh_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 10.137092735956662 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.009013291645003425 * cm**-3, 'T': 5623413.251903491 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55mh+_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 18.026583290006865 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.012019417582950692 * cm**-3, 'T': 7498942.093324558 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '55h_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 32.056301897970904 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.01602815094898545 * cm**-3, 'T': 10000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e55 * Hz,
        }
    },
    '56l_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 0.320563018979709 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0016028150948985452 * cm**-3, 'T': 1000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e56 * Hz,
        }
    },
    '56lm_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 1.013709273595666 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0028502530813576637 * cm**-3, 'T': 1778279.410038923 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e56 * Hz,
        }
    },
    '56m_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 3.205630189797091 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.005068546367978328 * cm**-3, 'T': 3162277.6601683795 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e56 * Hz,
        }
    },
    '56mh_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 10.137092735956662 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.009013291645003425 * cm**-3, 'T': 5623413.251903491 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e56 * Hz,
        }
    },
    '56h_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 32.056301897970904 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.01602815094898545 * cm**-3, 'T': 10000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e56 * Hz,
        }
    },
    '57l_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 0.320563018979709 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0016028150948985452 * cm**-3, 'T': 1000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e57 * Hz,
        }
    },
    '57lm_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 1.013709273595666 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.0028502530813576637 * cm**-3, 'T': 1778279.410038923 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e57 * Hz,
        }
    },
    '57m_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 3.205630189797091 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.005068546367978328 * cm**-3, 'T': 3162277.6601683795 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e57 * Hz,
        }
    },
    '57mh_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 10.137092735956662 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.009013291645003425 * cm**-3, 'T': 5623413.251903491 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.00001,
            'L': 1e57 * Hz,
        }
    },
    '57h_tilted_2d': {
        'simulation': {'z': 3.0, 'base_grid': 640, 'length': 640 * pc},
        'clump': {
            'd': 100 * kpc, 'r': 50 * pc, 'h': 2.5 * pc,
            'x': 320 * pc, 'y': 320 * pc,
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,
            'ntr_frac_0': 1, 'ntr_frac_1': 1, 'ntr_frac_2': 0,
            'n': 32.056301897970904 * cm**-3, 'T': 5000 * K,
        },
        'halo': {
            'ntr_frac_0': 0, 'ntr_frac_1': 0, 'ntr_frac_2': 0,
            'n': 0.01602815094898545 * cm**-3, 'T': 10000000.0 * K,
        },
        'QSO': {
            'a': -1.7, 'scaled_x': - (10.0 / sqrt(2)) + .0001,
            'scaled_y': - (10.0 / sqrt(2)) + .0001, 'scaled_z': 0.5001,
            'L': 1e57 * Hz,
        }
    },
}


def main():
    print('Here\'s the list of pre-set simulations:')
    print(list(PRESET_SIMS.keys()) + ['all'])

    selected_preset = ''
    while selected_preset not in list(PRESET_SIMS.keys()) + ['all']:
        selected_preset = input(
            'Please enter the name of your preferred simulation: ')

    if selected_preset == 'all':
        selected_sims = PRESET_SIMS
    else:
        selected_sims = {selected_preset: PRESET_SIMS[selected_preset]}

    for simname, sim in selected_sims.items():
        print(simname)
        params = new_params()
        for cat, attrs in sim.items():
            for attr, val in attrs.items():
                params[cat][attr] = val

        params['simulation']['id'] = simname

        fill_parameters(params)

        directory = 'L{0:.2f}Hz-{1}/{2}'.format(
            log10(params['QSO']['L'].to(Hz).value), sim['simulation']['base_grid'], simname)
        Path(directory).mkdir(parents=True, exist_ok=True)

        param_file = fill_param_file(params)

        with open(directory + '/' + params['simulation']['id'] + '.txt', 'w') as f:
            for title, cat in params.items():
                for key, value in cat.items():
                    f.write(
                        '# {}/{} = {}\n'.format(title, key, str(value)))
            f.write('\n')

            for title, cat in param_file.items():
                f.write('# {}\n'.format(title))
                for key, value in cat.items():
                    f.write('{} = {}\n'.format(key, value))
                f.write('\n')


def filename(params):
    L_qso = params['QSO']['L']
    n_halo = params['halo']['n']
    T_halo = params['halo']['T']
    d_clump = params['clump']['d']
    r_clump = params['clump']['r']
    n_clump = params['clump']['n']
    T_clump = params['clump']['T']

    if params['simulation']['id']:
        fname = params['simulation']['id'].replace('+', 'e')
    else:
        fname = "L{0:.2f}Hz_d{1:d}kpc_Hn{2:.2e}_HT{3:.2e}_Cr{4:d}pc_Cn{5:.2e}_CT{6:.2e}".format(
                log10(L_qso.to(Hz).value),
                int(d_clump.to(kpc).value),
                n_halo.to(cm**-3).value,
                T_halo.to(K).value,
                int(r_clump.to(pc).value),
                n_clump.to(cm**-3).value,
                T_clump.to(K).value
        ).replace(' ', '').replace('.', '_').replace('+', 'p')

    return fname


def new_params():
    return {
        'simulation': {
            'X': .75, 'Y': .25,
            'elements': 'H He',
            'z': 3.0,
            'base_grid': 640,
            'length': 640 * pc,
            'nsteps': 100000, 'output_frequency': 25,
            'CaseA': True,
            'max_rt_timestep': 1 * Myr, 'min_rt_timestep': 0.05 * yr,
            'Ob': None, 'Om': None, 'Ol': None, 'H0': None,  # TBF
            'filename': None, 'id': None,
        },
        'clump': {
            'd': 100. * kpc,  # distance
            'r': 50 * pc, 'h': 2.5 * pc,  # Radius and smoothing width
            'x': 320 * pc, 'y': 320 * pc,  # coordinates
            'u': 0 * Mpc / Myr, 'v': 0 * Mpc / Myr, 'w': 0 * Mpc / Myr,  # vel
            'n': 32.056301897970904 * cm**-3,  # number density
            'T': None,
            'ntr_frac_0': 1,  # Hydrogen neutral fraction
            'ntr_frac_1': 1,  # Helium neutral fraction
            'ntr_frac_2': 0,  # Helium+1 neutral fraction
            'mu': None, 'rho': None, 'p': None,
        },
        'halo': {
            'n': 0.01602815094898545 * cm**-3,
            'T': 10e6 * K,
            'ntr_frac_0': 0,  # Hydrogen neutral fraction
            'ntr_frac_1': 0,  # Helium neutral fraction
            'ntr_frac_2': 0,  # Helium+1 neutral fraction
            'mu': None,  # TBC
            'rho': None,  # TBC
            'p': None,  # TBC
        },
        'QSO': {
            'L': 1e57 * Hz,
            'log10_L': None,
            'a': -1.7,
            'scaled_x': - (10.0 / sqrt(2)) + .0001,  # box size
            'scaled_y': - (10.0 / sqrt(2)) + .0001,  # box size
            'scaled_z': 0.00001,  # box size
            'scaled_L': None,
            'log10_scaled_L': None,
            'scaled_clump_d': None,
        },
        'WGN': {
            'method': 'Box-Muller',
            'variable': 'rho',
            'range': [0.5, 2],  # in unit of the clump values
            'random seed': 19890522,
            'standard deviation': 1e0,
            'mean': 0e0,
            'amplitude': 30e0,
        },
    }


def fill_parameters(params):
    params['simulation']['Ob'] = cosmo.Ob(0)
    params['simulation']['Om'] = cosmo.Om(0)
    params['simulation']['Ol'] = cosmo.Ode(0)
    params['simulation']['H0'] = cosmo.H(0).to(km / s / Mpc)

    X = params['simulation']['X']
    Y = params['simulation']['Y']

    T_halo = params['halo']['T']
    n_halo = params['halo']['n']
    n_clump = params['clump']['n']

    params['clump']['T'] = T_clump = T_halo * n_halo / n_clump
    params['clump']['mu'] = mu_clump = (X / 1 + Y / 4)**-1
    params['clump']['rho'] = n_clump * mu_clump
    params['clump']['p'] = (
        n_clump * k_B / m_p * T_clump
    ).to(cm**-3 * Mpc**2 / Myr**2)

    params['halo']['mu'] = mu_halo = \
        (X / 1 + Y / 4 + X / 1 + 2*Y / 4)**-1
    params['halo']['rho'] = n_halo * mu_halo
    params['halo']['p'] = (
        n_halo * k_B / m_p * T_halo
    ).to(cm**-3 * Mpc**2 / Myr**2)

    L = params['QSO']['L']
    l_box = params['simulation']['length']
    x_scaled_QSO = params['QSO']['scaled_x'] * l_box
    y_scaled_QSO = params['QSO']['scaled_y'] * l_box
    z_scaled_QSO = params['QSO']['scaled_z'] * l_box
    x_clump = params['clump']['x']
    y_clump = params['clump']['y']
    clump_d = params['clump']['d']

    params['QSO']['scaled_clump_d'] = sqrt(
        (x_clump - x_scaled_QSO).to(kpc).value ** 2
        + (y_clump - y_scaled_QSO).to(kpc).value**2
        + (0 - z_scaled_QSO).to(kpc).value**2
    ) * kpc

    params['QSO']['log10_L'] = log10(L.to(Hz).value)
    params['QSO']['scaled_L'] = L * \
        ((params['QSO']['scaled_clump_d'].to(kpc) / clump_d.to(kpc)))**2
    params['QSO']['log10_scaled_L'] = log10(
        params['QSO']['scaled_L'].to(Hz).value)

    fname = filename(params)
    params['simulation']['filename'] = fname
    params['simulation']['id'] = fname

    params['WGN']['range'][0] *= params['clump']['n'].to(cm**-3).value
    params['WGN']['range'][1] *= params['clump']['n'].to(cm**-3).value


def new_param_file():
    return {
        'species': {
            'NSpecies': 3,
        },
        'general': {
            'ActualRun': '.true.',
            'VersionID': None
        },
        'initial evolution': {
            'DoInitEvol': '.false.',
            'InitCollEq': '.false.',
        },
        'optimization': {
            #  'ATSFact': '1e1',
            #  'ATSFreq': '10',
            #  'ATSMaxNtrFrac': '9d-1',
            #  'ATSDensityThr': None,
            #  'ReactivateCell': '.true.',
            'MinTau': '0.01 0.01 0.01',
            'MinTauDiff': '0.01 0.01 0.01',
            'MinTauCell': '0.01 0.01 0.01',
        },
        'light speed params': {
            'LightSpeedLimit': '.false.',
        },
        'Recombination': {
            'CaseA': None,
            'ClumpingFactor': '1e0',
        },
        'UVB': {
            'InitUVBEq': '.false.',
            'UVB_SS': '.false.',
            'KeepBKG': '.false.',
        },
        #  'output': {
        #      'NTimeSteps': None,
        #      'OutputFreqStep': None,
        #  },
        'log': {
            'Verbosity': '3',
        },
        'timestep': {
            'TimeStepFact': '0.2',
            'InitialSimTime': '0.0',
            'SimulationTime': '20  # Myr',
            'MaxTimeStep': None,
            'MinTimeStep': None,
        },
        'cosmology': {
            'OmegaBaryonNow': None,
            'OmegaCDMNow': None,
            'OmegaLambdaNow': None,
            'HubbleConstNow': None,
            'InitialRedshift': None,
        },
        'ic': {
            'InitHIIradius': None,  # MaxDist
            'InitHIITemp': '1e4  # K',
            'SafetyDist': None,
        },
        'Heating & Cooling mechanisms': {
            'IncludeCollIon': '1',
            'IncludeCollExc': '1',
            'IncludeHubbleCool': '0',
            'IncludeComptCool': '0',
            'IncludeQSOComptHeat': '.true.',
        },
        'Single source': {
            'NSources': '1',
            'SourceCoords': None,
            'SpectrumType': '1',
            'TotalIonRate': None,
            'SpectrumSlope': None,
            'OpAngle': '0.0',
            'NSpectralRegions': '3',
            'NSpectralBins': '10 10 50',
            'MinMaxEnergy': '1 1.8 1.8 4 4 100',
        },
        'Logger': {
            'unicode_plotting': 'enable',
            'projection_axis': 'z',
            'colormap': 'magma_grey',
        },
        'Report': {
            'report_pseudocolor': 'rho',
            'report_pseudocolor ': 'ntr_frac_0',
            'report_pseudocolor  ': 'temp',
            'report_frequency': 5,
        },
        'Ray tracing': {
            'NRays': '5',
            'MaxTau': '50 50 50',
        },
        'Initial Condition': {
            'ic_type': 'simple',
            'ic_grid': None,
            'ic_box_lengths': None,  # "kpc"
            'ic_redshift': None,
        },
        'AMR': {
            'ic_nlevels': '1',
            'max_nboxes': '1',
        },
        'Internal Units': {
            'density_unit': '"m_H / cm^3"',
            'length_unit': '"Mpc"',
            'time_unit': '"Myr"',
        },
        'Boundary Conditions': {
            'left_bc': 'outflow',
            'right_bc': 'outflow',
            'bottom_bc': 'outflow',
            'top_bc': 'outflow',
            'back_bc': 'outflow',
            'front_bc': 'outflow',
        },
        'Thermodynamics': {
            'ideal_gas_type': 'monatomic',
        },
        'CFL': {
            'courant_number': '0.2  # Better to choose a value less than 0.2',
        },
        'Exact Riemann Solver': {
            'vacuum_pressure': None,
            'vacuum_density': None,
            'tolerance': '1.d-8',
            'n_iteration': '10000',
        },
        'Slope Limiter': {
            'slope_limiter': 'minmod',
            'slope_limiter_omega': '0d0',
        },
        'Chemistry': {
            'elements': 'H He',
            'element_abundances': None,
        },
        'Ionisation Equilibrium': {
            'uvb_equilibrium': 'disable',
            'uvb_model': 'HM12',
            'uvb_self_shielding': 'disable',
            'collisional_ionization_equilibrium': 'enable',
            'photoionization_equilibrium': 'disable',
            'ie_convergence_rate': 1e-2,
            'ie_max_niterations': 10000,
            'species_cases': 'case_a case_a case_a',
            'equilibrium_table_size': '2048 2048',
            'equilibrium_table_temp_range': '1d3 1d8 K',
            'equilibrium_table_density_range': '1d-4 1d3 "m_H / cm^3"',
        },
        'MUSCL-Hancock solver': {
            'solver_type': 'memory_intensive',
        },
        'Drawing': {
            'canvas': None,
            'shape': None,
            'shape_filling': None,
            'perturb': None,
        },
        'Sanity Check': {
            'sanity_check': 'enable',
            'sanity_check_frequency': 1,
            'sanity_check_rho': None,
            'sanity_check_e_tot': None,
            'sanity_check_temp': None,
            'sanity_check_ntr_frac_0': 'enable 0 1',
            'sanity_check_ntr_frac_1': 'enable 0 1',
            'sanity_check_ntr_frac_2': 'enable 0 1',
            'sanity_check_total_mass': 'enable 0.9 1.1',
            'sanity_check_total_energy': 'enable 0.9 1.1',
        },
        'Stabilizer': {
            'stabilizer': 'disable',
        },
        'Chombo Output': {
            'chombo_prefix': None,
            'chombo_nickname': None,
            'chombo_output_every': -1,
            'chombo_output_restart_backup_every': 10,
            'chombo_output_rule': 'log 1d-6 1d-1 40',
            'chombo_output_rule ': 'linear 2d-1 6d1 599',
            'chombo_output_final_time ': '40.1',
        },
    }


def fill_param_file(params):
    param_file = new_param_file()

    z = params['simulation']['z']
    CaseA = params['simulation']['CaseA']
    nsteps = params['simulation']['nsteps']
    output_frequency = params['simulation']['output_frequency']
    boxsize_pc = params['simulation']['length'].to(pc).value
    basegrid = params['simulation']['base_grid']
    X = float(params['simulation']['X'])
    Y = float(params['simulation']['Y'])

    max_rt_timestep = params['simulation']['max_rt_timestep']
    min_rt_timestep = params['simulation']['min_rt_timestep']

    Ob = params['simulation']['Ob']
    Om = params['simulation']['Om']
    Ol = params['simulation']['Ol']
    H0 = params['simulation']['H0']

    halo_p = params['halo']['p'].to(cm**-3 * Mpc**2 / Myr**2)
    halo_rho = params['halo']['rho'].to(cm**-3)
    halo_T = params['halo']['T'].to(K)
    halo_ntr_frac_0 = params['halo']['ntr_frac_0']
    halo_ntr_frac_1 = params['halo']['ntr_frac_1']
    halo_ntr_frac_2 = params['halo']['ntr_frac_2']

    clump_rho = params['clump']['rho'].to(cm**-3)
    clump_p = params['clump']['p'].to(cm**-3 * Mpc**2 / Myr**2)
    clump_x = params['clump']['x'].to(kpc)
    clump_y = params['clump']['y'].to(kpc)
    clump_u = params['clump']['u'].to(Mpc / Myr)
    clump_v = params['clump']['v'].to(Mpc / Myr)
    clump_w = params['clump']['w'].to(Mpc / Myr)
    clump_r = params['clump']['r'].to(kpc)
    clump_h = params['clump']['h'].to(kpc)
    clump_T = params['clump']['T'].to(K)
    clump_ntr_frac_0 = params['clump']['ntr_frac_0']
    clump_ntr_frac_1 = params['clump']['ntr_frac_1']
    clump_ntr_frac_2 = params['clump']['ntr_frac_2']

    param_file['general']['VersionID'] = params['simulation']['id']

    #  param_file['optimization']['ATSDensityThr'] = '{}  # over-density'.format(
    #      halo_n.to(cm**-3).value /
    #      (cosmo.Ob(z) * cosmo.critical_density(z) / m_p).to(cm**-3).value
    #  )

    param_file['Recombination']['CaseA'] = '.ture.' if CaseA else '.false.'

    param_file['timestep']['MaxTimeStep'] = '{}  # Myr'.format(
        max_rt_timestep.to(Myr).value)
    param_file['timestep']['MinTimeStep'] = '{}  # Myr'.format(
        min_rt_timestep.to(Myr).value)

    param_file['cosmology']['OmegaBaryonNow'] = Ob
    param_file['cosmology']['OmegaCDMNow'] = Om
    param_file['cosmology']['OmegaLambdaNow'] = Ol
    param_file['cosmology']['HubbleConstNow'] = '{}  # km / s / Mpc'.format(
        H0.to(km / s / Mpc).value)
    param_file['cosmology']['InitialRedshift'] = z

    param_file['ic']['InitHIIradius'] = '{}  # box size'.format((
        params['QSO']['scaled_clump_d'].to(kpc).value
        / params['simulation']['length'].to(kpc).value
    ) + sqrt(3.))
    param_file['ic']['SafetyDist'] = param_file['ic']['InitHIIradius']

    param_file['Single source']['SourceCoords'] = \
        "{} {} {}  # box length".format(
        params['QSO']['scaled_z'],
        params['QSO']['scaled_x'],
        params['QSO']['scaled_y'])
    param_file['Single source']['TotalIonRate'] = "{}  # {} Hz".format(
        params['QSO']['log10_scaled_L'], params['QSO']['L'].to(Hz).value)
    param_file['Single source']['SpectrumSlope'] = '{}  # {}'.format(
        -params['QSO']['a'], params['QSO']['a'])

    param_file['Initial Condition']['ic_grid'] = "{0} {0}".format(
        params['simulation']['base_grid'])
    param_file['Initial Condition']['ic_box_lengths'] = \
        "{0} {0} 'kpc'".format(
        params['simulation']['length'].to(kpc).value)
    param_file['Initial Condition']['ic_redshift'] = params['simulation']['z']

    param_file['Exact Riemann Solver']['vacuum_pressure'] = \
        '{}  # cm^-3 Mpc^2 Myr^-2'.format(
        halo_p.to(cm**-3 * Mpc**2 / Myr**2).value * 10**-20)
    param_file['Exact Riemann Solver']['vacuum_density'] = \
        '{}  # cm^-3'.format(halo_rho.to(cm**-3).value * 10**-20)

    param_file['Chemistry']['element_abundances'] = '{} {}'.format(X, Y)

    param_file['Drawing']['canvas'] = \
        "uniform {} 0d0 0d0 {} {} {} {} {}  # rho u v p T fHI fHeI fHeII".format(
        halo_rho.to(cm**-3).value, halo_p.to(cm**-3 * Mpc**2 / Myr**2).value,
        halo_T.to(K).value, halo_ntr_frac_0, halo_ntr_frac_1, halo_ntr_frac_2)
    param_file['Drawing']['shape'] = \
        "sphere {} {} {} {} 'kpc'  # x y r h".format(
        clump_x.to(kpc).value, clump_y.to(kpc).value,
        clump_r.to(kpc).value, clump_h.to(kpc).value)
    param_file['Drawing']['shape_filling'] = \
        "uniform absolute {} {} {} {} {} {} {} {} {} 0d0 0d0 {} {} {} {} {}".format(
        clump_rho.to(cm**-3).value, clump_u.to(Mpc / Myr).value,
        clump_v.to(Mpc / Myr).value,
        clump_p.to(cm**-3 * Mpc**2 / Myr**2).value, clump_T.to(K).value,
        clump_ntr_frac_0, clump_ntr_frac_1, clump_ntr_frac_2,
        halo_rho.to(cm**-3).value, halo_p.to(cm**-3 * Mpc**2 / Myr**2).value,
        halo_T.to(K).value, halo_ntr_frac_0, halo_ntr_frac_1, halo_ntr_frac_2)
    param_file['Drawing']['perturb'] = \
        "wgn {} {} {} {} {} {} {} {}".format(
            params['WGN']['method'].replace('-', '_').lower(),
            params['WGN']['random seed'],
            params['WGN']['variable'],
            params['WGN']['range'][0],
            params['WGN']['range'][1],
            params['WGN']['standard deviation'],
            params['WGN']['mean'],
            params['WGN']['amplitude'],
    )

    param_file['Sanity Check']['sanity_check_rho'] = 'enable {} {} {}'.format(
        halo_rho.to(cm**-3).value / 3, clump_rho.to(cm**-3).value * 3, '"m_H / cm^3"')
    param_file['Sanity Check']['sanity_check_e_tot'] = 'enable {} {} {}'.format(
        0.0, 3 * (halo_p / (5. / 3. - 1)
                  ).to(cm**-3 * Mpc**2 / Myr**2).value,
        '"m_H / cm^3 * Mpc^2 / Myr^2"')
    param_file['Sanity Check']['sanity_check_temp'] = 'enable {} {} {}'.format(
        clump_T.to(K).value / 2, 2 * halo_T.to(K).value, 'K')

    param_file['Chombo Output']['chombo_prefix'] = '\"./output\"'
    param_file['Chombo Output']['chombo_nickname'] = params['simulation']['id']

    return param_file


if __name__ == '__main__':
    main()
