# simulation/X = 0.75
# simulation/Y = 0.25
# simulation/elements = H He
# simulation/z = 3.0
# simulation/base_grid = 160
# simulation/length = 640.0 pc
# simulation/nsteps = 100000
# simulation/output_frequency = 25
# simulation/CaseA = True
# simulation/max_rt_timestep = 1.0 Myr
# simulation/min_rt_timestep = 0.05 yr
# simulation/Ob = 0.0486
# simulation/Om = 0.3075
# simulation/Ol = 0.6910098315261186
# simulation/H0 = 67.74 km / (Mpc s)
# simulation/filename = 55mh_tilted_2d
# simulation/id = 55mh_tilted_2d
# clump/d = 100.0 kpc
# clump/r = 50.0 pc
# clump/h = 2.5 pc
# clump/x = 320.0 pc
# clump/y = 320.0 pc
# clump/u = 0.0 Mpc / Myr
# clump/v = 0.0 Mpc / Myr
# clump/w = 0.0 Mpc / Myr
# clump/n = 10.137092735956662 1 / cm3
# clump/T = 4999.999999999997 K
# clump/ntr_frac_0 = 1
# clump/ntr_frac_1 = 1
# clump/ntr_frac_2 = 0
# clump/mu = 1.2307692307692308
# clump/rho = 12.476421828869737 1 / cm3
# clump/p = 4.3759844021094077e-10 Mpc2 / (cm3 Myr2)
# halo/n = 0.009013291645003425 1 / cm3
# halo/T = 5623413.251903491 K
# halo/ntr_frac_0 = 0
# halo/ntr_frac_1 = 0
# halo/ntr_frac_2 = 0
# halo/mu = 0.5925925925925926
# halo/rho = 0.005341209863705733 1 / cm3
# halo/p = 4.3759844021094077e-10 Mpc2 / (cm3 Myr2)
# QSO/L = 1e+55 Hz
# QSO/log10_L = 55.0
# QSO/a = -1.7
# QSO/scaled_x = -7.070967811865475
# QSO/scaled_y = -7.070967811865475
# QSO/scaled_z = 1e-05
# QSO/scaled_L = 4.6956178315962856e+52 Hz
# QSO/log10_scaled_L = 52.671692743183584
# QSO/scaled_clump_d = 6.852457830294387 kpc
# WGN/method = Box-Muller
# WGN/variable = rho
# WGN/range = [5.068546367978331, 20.274185471913324]
# WGN/random seed = 19890522
# WGN/standard deviation = 1.0
# WGN/mean = 0.0
# WGN/amplitude = 30.0

# species
NSpecies = 3

# general
ActualRun = .true.
VersionID = 55mh_tilted_2d

# initial evolution
DoInitEvol = .false.
InitCollEq = .false.

# optimization
MinTau = 0.01 0.01 0.01
MinTauDiff = 0.01 0.01 0.01
MinTauCell = 0.01 0.01 0.01

# light speed params
LightSpeedLimit = .false.

# Recombination
CaseA = .ture.
ClumpingFactor = 1e0

# UVB
InitUVBEq = .false.
UVB_SS = .false.
KeepBKG = .false.

# log
Verbosity = 3

# timestep
TimeStepFact = 0.2
InitialSimTime = 0.0
SimulationTime = 20  # Myr
MaxTimeStep = 1.0  # Myr
MinTimeStep = 5e-08  # Myr

# cosmology
OmegaBaryonNow = 0.0486
OmegaCDMNow = 0.3075
OmegaLambdaNow = 0.6910098315261186
HubbleConstNow = 67.74  # km / s / Mpc
InitialRedshift = 3.0

# ic
InitHIIradius = 12.439016167403857  # box size
InitHIITemp = 1e4  # K
SafetyDist = 12.439016167403857  # box size

# Heating & Cooling mechanisms
IncludeCollIon = 1
IncludeCollExc = 1
IncludeHubbleCool = 0
IncludeComptCool = 0
IncludeQSOComptHeat = .true.

# Single source
NSources = 1
SourceCoords = 1e-05 -7.070967811865475 -7.070967811865475  # box length
SpectrumType = 1
TotalIonRate = 52.671692743183584  # 1e+55 Hz
SpectrumSlope = 1.7  # -1.7
OpAngle = 0.0
NSpectralRegions = 3
NSpectralBins = 10 10 50
MinMaxEnergy = 1 1.8 1.8 4 4 100

# Logger
unicode_plotting = enable
projection_axis = z
colormap = magma_grey

# Report
report_pseudocolor = rho
report_pseudocolor  = ntr_frac_0
report_pseudocolor   = temp
report_frequency = 5

# Ray tracing
NRays = 5
MaxTau = 50 50 50

# Initial Condition
ic_type = simple
ic_grid = 160 160
ic_box_lengths = 0.64 0.64 'kpc'
ic_redshift = 3.0

# AMR
ic_nlevels = 1
max_nboxes = 1

# Internal Units
density_unit = "m_H / cm^3"
length_unit = "Mpc"
time_unit = "Myr"

# Boundary Conditions
left_bc = outflow
right_bc = outflow
bottom_bc = outflow
top_bc = outflow
back_bc = outflow
front_bc = outflow

# Thermodynamics
ideal_gas_type = monatomic

# CFL
courant_number = 0.2  # Better to choose a value less than 0.2

# Exact Riemann Solver
vacuum_pressure = 4.375984402109407e-30  # cm^-3 Mpc^2 Myr^-2
vacuum_density = 5.341209863705733e-23  # cm^-3
tolerance = 1.d-8
n_iteration = 10000

# Slope Limiter
slope_limiter = minmod
slope_limiter_omega = 0d0

# Chemistry
elements = H He
element_abundances = 0.75 0.25

# Ionisation Equilibrium
uvb_equilibrium = disable
uvb_model = HM12
uvb_self_shielding = disable
collisional_ionization_equilibrium = enable
photoionization_equilibrium = disable
ie_convergence_rate = 0.01
ie_max_niterations = 10000
species_cases = case_a case_a case_a
equilibrium_table_size = 2048 2048
equilibrium_table_temp_range = 1d3 1d8 K
equilibrium_table_density_range = 1d-4 1d3 "m_H / cm^3"

# MUSCL-Hancock solver
solver_type = memory_intensive

# Drawing
canvas = uniform 0.005341209863705733 0d0 0d0 4.3759844021094077e-10 5623413.251903491 0 0 0  # rho u v p T fHI fHeI fHeII
shape = sphere 0.32 0.32 0.05 0.0025 'kpc'  # x y r h
shape_filling = uniform absolute 12.476421828869737 0.0 0.0 4.3759844021094077e-10 4999.999999999997 1 1 0 0.005341209863705733 0d0 0d0 4.3759844021094077e-10 5623413.251903491 0 0 0
perturb = wgn box_muller 19890522 rho 5.068546367978331 20.274185471913324 1.0 0.0 30.0

# Sanity Check
sanity_check = enable
sanity_check_frequency = 1
sanity_check_rho = enable 0.0017804032879019111 37.42926548660921 "m_H / cm^3"
sanity_check_e_tot = enable 0.0 1.9691929809492335e-09 "m_H / cm^3 * Mpc^2 / Myr^2"
sanity_check_temp = enable 2499.9999999999986 11246826.503806982 K
sanity_check_ntr_frac_0 = enable 0 1
sanity_check_ntr_frac_1 = enable 0 1
sanity_check_ntr_frac_2 = enable 0 1
sanity_check_total_mass = enable 0.9 1.1
sanity_check_total_energy = enable 0.9 1.1

# Stabilizer
stabilizer = disable

# Chombo Output
chombo_prefix = "./output"
chombo_nickname = 55mh_tilted_2d
chombo_output_every = -1
chombo_output_restart_backup_every = 10
chombo_output_rule = log 1d-6 1d-1 20
chombo_output_rule  = linear 2d-1 6d1 599
chombo_output_final_time  = 40.1

