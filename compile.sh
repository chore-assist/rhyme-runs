#!/usr/bin/env bash

source ./filenames.sh
source ./logging.sh


compile_usage() {
  echo 'USAGE: ./compile.sh hydro_flag rt_flag [X_HI [X_HeI]]'
}


main() {
  if [[ -n "$1" ]]; then local hydro_flag=$1; else "$(compile_usage)"; exit; fi
  if [[ -n "$2" ]]; then local rt_flag=$2; else "$(compile_usage)"; exit; fi
  if [[ -n "$3" ]]; then local X_HI="$3"; else X_HI='1d0'; fi
  if [[ -n "$4" ]]; then local X_HeI="$4"; else X_HeI='0d0'; fi

  log ${hydro_flag} "Hydro Flag"
  log ${rt_flag} "RT Flag"
  log ${X_HI} "X_HI"
  log ${X_HeI} "X_HeI"

  exe="$(radamesh_hydro_exe ${hydro_flag} ${rt_flag} ${X_HI} ${X_HeI})"
  log ${exe} "exe"


  log $(pwd) 'Wodking directory'
  wd=$(pwd)

  log 'Chaning workin directory to radamesh-hydro'
  cd radamesh-hydro

  log 'Replacing X_HI & X_HeI placeholder of GlobalModule.f90'
  global_module='src/radamesh/src/GlobalModule.f90'
  global_module_md='src/radamesh/src/GlobalModule.f90.md'

  log 'Taking a backup of the original GlobalModule.f90 file'
  if [[ -f ${global_module_md} ]]; then
    cp ${global_module_md} ${global_module}
  else
    cp ${global_module} ${global_module_md}
  fi

  log 'Replacing X_HI & X_HeI'
  sed -i "s/<HFrac>/${X_HI}/g" ${global_module}
  sed -i "s/<dHFrac>/${X_HI}/g" ${global_module}
  sed -i "s/<HeFrac>/${X_HeI}/g" ${global_module}
  sed -i "s/<dHeFrac>/${X_HeI}/g" ${global_module}

  log 'Printing changes'
  colordiff ${global_module_md} ${global_module}


  log 'Building RadameshHydro'
  log '[TODO] Use hydro_flag & rt_flag to generate proper executables'
  mkdir -pv build
  cd build
  cmake .. && make && ctest --timeout 3 --output-on-failure

  mv -v ${RADAMESH_HYDRO_BASE_NAME} ${exe}
}


main "$@"
